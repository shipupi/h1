@extends('adminlte::layouts.app')

@section('htmlheader_title')
	SOBRE HANGAR
@endsection

@section('contentheader_title')
	SOBRE HANGAR - Texto
@endsection
@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- /.box -->
				{!! Form::open(['url' => 'admin/updateMeta', 'files' => true]) !!}
				{!! Form::hidden('redirect' , 'admin/about/text') !!}

				<!-- Quienes Somos Main Izquierda -->
				<div class="form-group">
					{!! Form::label('about_text', 'Sobre Hangar Uno') !!}
					{!! Form::textarea('about_text', getMeta('about_text')) !!}
				</div>

				<!-- Quienes Somos Main Izquierda -->
				<div class="form-group">
					{!! Form::label('about_text_english', 'Sobre Hangar Uno (Ingles)') !!}
					{!! Form::textarea('about_text_english', getMeta('about_text_english')) !!}
				</div>

				<div class="form-group">
					{!! Form::submit('Enviar') !!}
				</div>
				{!! Form::close() !!}

			</div>
		</div>
	</div>
@endsection


@section('custom_scripts')

<script type="text/javascript">
	$(function(){ 
		CKEDITOR.replace( 'about_text' );
		CKEDITOR.replace( 'about_text_english' );
	})
</script>

@endsection