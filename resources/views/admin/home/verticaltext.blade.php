@extends('adminlte::layouts.app')

@section('htmlheader_title')
	SOBRE HANGAR
@endsection

@section('contentheader_title')
@endsection
@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- /.box -->
				{!! Form::open(['url' => 'admin/updateMeta', 'files' => true]) !!}
				{!! Form::hidden('redirect' , 'admin/home/verticalText') !!}

				<!-- Quienes Somos Main Izquierda -->
				<div class="form-group">
					{!! Form::label('home_vertical_text', 'Texto vertical') !!}
					{!! Form::text('home_vertical_text', getMeta('home_vertical_text')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('home_vertical_text_english', 'Texto vertical (Ingles)') !!}
					{!! Form::text('home_vertical_text_english', getMeta('home_vertical_text_english')) !!}
				</div>

				<div class="form-group">
					{!! Form::submit('Enviar') !!}
				</div>
				{!! Form::close() !!}

			</div>
		</div>
	</div>
@endsection


@section('custom_scripts')


@endsection