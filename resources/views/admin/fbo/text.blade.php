@extends('adminlte::layouts.app')

@section('htmlheader_title')
	SOBRE HANGAR
@endsection

@section('contentheader_title')
	SOBRE HANGAR - Texto
@endsection
@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- /.box -->
				{!! Form::open(['url' => 'admin/updateMeta', 'files' => true]) !!}
				{!! Form::hidden('redirect' , 'admin/fbo/text') !!}

				<!-- Quienes Somos Main Izquierda -->
				<div class="form-group">
					{!! Form::label('fbo_text', 'FBO') !!}
					{!! Form::textarea('fbo_text', getMeta('fbo_text')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('fbo_text_english', 'FBO (Ingles)') !!}
					{!! Form::textarea('fbo_text_english', getMeta('fbo_text_english')) !!}
				</div>

				<div class="form-group">
					{!! Form::submit('Enviar') !!}
				</div>
				{!! Form::close() !!}

			</div>
		</div>
	</div>
@endsection


@section('custom_scripts')

<script type="text/javascript">
	$(function(){ 
		CKEDITOR.replace( 'fbo_text' );
		CKEDITOR.replace( 'fbo_text_english' );
	})
</script>

@endsection