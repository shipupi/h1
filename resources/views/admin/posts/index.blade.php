@extends('adminlte::layouts.app')

@section('htmlheader_title')
	
@endsection

@section('contentheader_title')
	Slider
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<!-- /.box -->
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Titulo</th>
					<th>Fecha</th>
					<th>Autor</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
		@foreach ($posts as $post)
				<tr>
					<td>{{$post->title}}</td>
					<td>{{$post->created_at}}</td>
					<td>{{$post->user->name}}</td>
					<td><a href="{{url('admin/posts/edit/' . $post->id)}}"><button class="btn btn-info">Editar</button></a></td>
					<td><a href="{{url('admin/posts/delete/' . $post->id)}}"><button class="btn btn-danger">Eliminar</button></a></td>
				</tr>
		@endforeach
			</tbody>
		</table>

		<a href="{{url('admin/posts/create')}}"><button class="btn btn-success">Agregar post</button></a>
	</div>
@endsection


@section('custom_scripts')

<script type="text/javascript">
	$(function() {
	})
</script>

@endsection