@extends('adminlte::layouts.app')

@section('htmlheader_title')
	
@endsection

@section('contentheader_title')
	Crear Marca
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<!-- /.box -->
		<div class="text-center"><h1>Editar post</h1></div>
		{!! Form::open(['url' => 'admin/posts/update/'. $post->id, 'files' => true]) !!}
			<div class="form-group">
				{!! Form::label('title', 'Titulo') !!}
				{!! Form::text('title', $post->title) !!}
			</div>
			<div class="form-group row">
				<div class="col-xs-6">
				    {!! Form::label('Imagen') !!}
				    {!! Form::file('image', null) !!}
				</div>
				<div class="col-xs-6">
					<img style="height:250px" src="{{asset($post->image)}}">
				</div>
			</div>

			<div class="form-group">
			    {!! Form::label('Resumen') !!}
			    {!! Form::textarea('lede', $post->lede) !!}
			</div>
			<div class="form-group">
			    {!! Form::label('Contenido') !!}
			    {!! Form::textarea('content', $post->content) !!}
			</div>


			<div class="separator" style="height: 1px; border-top: 10px solid black;"></div>
			<h1>ENGLISH</h1>
			<div class="form-group">
				{!! Form::label('title', 'Titulo (Ingles)') !!}
				{!! Form::text('english_title', $post->english_title) !!}
			</div>
			<div class="form-group">
			    {!! Form::label('Resumen (Ingles))') !!}
			    {!! Form::textarea('english_lede', $post->english_lede) !!}
			</div>
			<div class="form-group">
			    {!! Form::label('Contenido (Ingles') !!}
			    {!! Form::textarea('english_content', $post->english_content) !!}
			</div>



			<div class="form-group text-center">
				{!! Form::submit('Enviar') !!}
			</div>

		{!! Form::close() !!}
	</div>
@endsection


@section('custom_scripts')

<script type="text/javascript">
	$(function(){ 
		CKEDITOR.replace( 'lede' );
		CKEDITOR.replace( 'content' );
		CKEDITOR.replace( 'english_lede' );
		CKEDITOR.replace( 'english_content' );
	})
</script>

@endsection