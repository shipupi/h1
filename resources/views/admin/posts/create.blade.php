@extends('adminlte::layouts.app')

@section('htmlheader_title')
	
@endsection

@section('contentheader_title')
	Crear Marca
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<!-- /.box -->
		<div class="text-center"><h1>Nuevo Post</h1></div>
		{!! Form::open(['url' => 'admin/posts/store', 'files' => true]) !!}
			<div class="form-group">
				{!! Form::label('title', 'Titulo') !!}
				{!! Form::text('title', null) !!}
			</div>
			<div class="form-group row">
				<div class="col-xs-12">
				    {!! Form::label('Imagen', 'image') !!}
				    {!! Form::file('image', null) !!}
				</div>
			</div>

			<div class="form-group">
			    {!! Form::label('Resumen') !!}
			    {!! Form::textarea('lede', null) !!}
			</div>
			<div class="form-group">
			    {!! Form::label('Contenido') !!}
			    {!! Form::textarea('content', null) !!}
			</div>


			<div class="separator" style="height: 1px; border-top: 10px solid black;"></div>
			<div class="text-center"><h1>ENGLISH</h1></div>
			<div class="form-group">
				{!! Form::label('title', 'Titulo (Ingles)') !!}
				{!! Form::text('english_title', null) !!}
			</div>
			<div class="form-group">
			    {!! Form::label('Resumen (Ingles))') !!}
			    {!! Form::textarea('english_lede', null) !!}
			</div>
			<div class="form-group">
			    {!! Form::label('Contenido (Ingles') !!}
			    {!! Form::textarea('english_content', null) !!}
			</div>



			<div class="form-group text-center">
				{!! Form::submit('Enviar') !!}
			</div>

		{!! Form::close() !!}
	</div>
@endsection


@section('custom_scripts')

<script type="text/javascript">
	$(function(){ 
		CKEDITOR.replace( 'lede' );
		CKEDITOR.replace( 'content' );
		CKEDITOR.replace( 'english_lede' );
		CKEDITOR.replace( 'english_content' );
	})
</script>

@endsection