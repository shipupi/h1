@extends('adminlte::layouts.app')

@section('htmlheader_title')
	
@endsection

@section('contentheader_title')
	Crear Marca
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<!-- /.box -->
		<div class="text-center"><h1>CAMBIAR PDF DE ENTRENAMIENTO</h1></div>
			{!! Form::open(['url' => 'admin/updateMeta', 'files' => true]) !!}
			{!! Form::hidden('redirect' , 'admin/entrenamiento/manual') !!}

			<div class="form-group">
			    {!! Form::label('PDF') !!}
			    {!! Form::file('entrenamientoPdf', null) !!}
			</div>

			<div class="form-group">
			    {!! Form::label('PDF (ingles)') !!}
			    {!! Form::file('entrenamientoPdf_english', null) !!}
			</div>


			<div class="form-group text-center">
				{!! Form::submit('Enviar') !!}
			</div>

		{!! Form::close() !!}
	</div>
@endsection


@section('custom_scripts')

<script type="text/javascript">
</script>

@endsection