@extends('adminlte::layouts.app')

@section('htmlheader_title')
	SOBRE HANGAR
@endsection

@section('contentheader_title')
	SOBRE HANGAR - Texto
@endsection
@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- /.box -->
				{!! Form::open(['url' => 'admin/updateMeta', 'files' => true]) !!}
				{!! Form::hidden('redirect' , 'admin/entrenamiento/text') !!}

				<!-- Quienes Somos Main Izquierda -->
				<div class="form-group">
					{!! Form::label('entrenamiento_text', 'Entrenamiento') !!}
					{!! Form::textarea('entrenamiento_text', getMeta('entrenamiento_text')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('entrenamiento_text_english', 'Entrenamiento (ingles)') !!}
					{!! Form::textarea('entrenamiento_text_english', getMeta('entrenamiento_text_english')) !!}
				</div>

				<div class="form-group">
					{!! Form::submit('Enviar') !!}
				</div>
				{!! Form::close() !!}

			</div>
		</div>
	</div>
@endsection


@section('custom_scripts')

<script type="text/javascript">
	$(function(){ 
		CKEDITOR.replace( 'entrenamiento_text' );
		CKEDITOR.replace( 'entrenamiento_text_english' );
	})
</script>

@endsection