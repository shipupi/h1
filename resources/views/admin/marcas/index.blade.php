@extends('adminlte::layouts.app')

@section('htmlheader_title')
	
@endsection

@section('contentheader_title')
	Slider
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<!-- /.box -->
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Orden</th>
					<th>Nombre</th>
					<th>Logo</th>
					<th>Logo Activo</th>
					<th>Modelos</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
		@foreach ($marcas as $marca)
				<tr>
					<td>{{$marca->weight}}</td>
					<td>{{$marca->name}}</td>
					<td><img style="width: 100px;" src="{{asset($marca->image)}}"></td>
					<td><img style="width: 100px;" src="{{asset($marca->image_active)}}"></td>
					<td>{{sizeof($marca->modelos)}}</td>
					<td><a href="{{url('admin/marcas/edit/' . $marca->id)}}"><button class="btn btn-info">Editar</button></a></td>
					<td><a href="{{url('admin/modelos/index/' . $marca->id)}}"><button class="btn btn-info">Modelos</button></a></td>
					<td><a href="{{url('admin/marcas/delete/' . $marca->id)}}"><button class="btn btn-danger">Eliminar</button></a></td>
				</tr>
		@endforeach
			</tbody>
		</table>

		<a href="{{url('admin/marcas/create')}}"><button class="btn btn-success">Agregar Marca</button></a>
	</div>
@endsection


@section('custom_scripts')

<script type="text/javascript">
	$(function() {
		$('body').on('click', '.remove-image', function() {
			var $this = $(this)
			var $row = $this.closest('.slider-image')
			var data  = {
				field: $row.data('field'),
				index: $row.data('index')
			}
			$.ajax({
				method: 'POST',
				url: "{{url('admin/removeMetaWithIndex')}}",
				data: data
			}).done(function(data){
				if (data == "ok") {
					$row.remove()
				}
			})
		})
	})
</script>

@endsection