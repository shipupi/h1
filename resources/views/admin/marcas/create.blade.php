@extends('adminlte::layouts.app')

@section('htmlheader_title')
	
@endsection

@section('contentheader_title')
	Crear Marca
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<!-- /.box -->
		<div class="text-center"><h1>CREAR MARCA</h1></div>
		{!! Form::open(['url' => 'admin/marcas/store', 'files' => true]) !!}
			<div class="form-group">
				{!! Form::label('name', 'Nombre') !!}
				{!! Form::text('name') !!}
			</div>
			<div class="form-group">
				{!! Form::label('weight', 'Orden') !!}
				{!! Form::number('weight') !!}
			</div>
			<div class="form-group">
				{!! Form::label('description', 'Descripcion') !!}
				{!! Form::textarea('description') !!}
			</div>
			<div class="form-group">
				{!! Form::label('english_description', 'Descripcion (Ingles)') !!}
				{!! Form::textarea('english_description') !!}
			</div>
			<div class="form-group">
			    {!! Form::label('Logo') !!}
			    {!! Form::file('image', null) !!}
			</div>

			<div class="form-group">
			    {!! Form::label('Logo Activo') !!}
			    {!! Form::file('image_active', null) !!}
			</div>


			<div class="form-group">
			    {!! Form::label('Imagen para home slider') !!}
			    {!! Form::file('slider_image', null) !!}
			</div>
			<div class="form-group">
				{!! Form::label('slider_link', 'Link para home slider') !!}
				{!! Form::text('slider_link') !!}
			</div>


			<div class="form-group text-center">
				{!! Form::submit('Enviar') !!}
			</div>

		{!! Form::close() !!}
	</div>
@endsection


@section('custom_scripts')

<script type="text/javascript">
	$(function(){ 
		CKEDITOR.replace( 'description' );
	})
</script>

@endsection