@extends('adminlte::layouts.app')

@section('htmlheader_title')
	
@endsection

@section('contentheader_title')
	Crear Marca
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<!-- /.box -->
		<div class="text-center"><h1>AGREGAR PRODUCTO</h1></div>
		{!! Form::open(['url' => 'admin/products/store', 'files' => true]) !!}
			<h1> Español </h1>
			<div class="form-group">
				{!! Form::label('name_spanish', 'Nombre (Español') !!}
				{!! Form::text('name_spanish') !!}
			</div>


			<div class="form-group">
			    {!! Form::label('Descripcion (Español') !!}
			    {!! Form::textarea('description_spanish', null) !!}
			</div>

			<h1> Ingles </h1>
			<div class="form-group">
				{!! Form::label('name_english', 'Nombre (Ingles') !!}
				{!! Form::text('name_english') !!}
			</div>

			<div class="form-group">
			    {!! Form::label('Descripcion (Ingles') !!}
			    {!! Form::textarea('description_english', null) !!}
			</div>

			<div class="form-group">
				{!! Form::label('weight', 'Orden') !!}
				{!! Form::number('weight') !!}
			</div>

			<div class="form-group text-center">
				{!! Form::submit('Enviar') !!}
			</div>

		{!! Form::close() !!}
	</div>
@endsection


@section('custom_scripts')

<script type="text/javascript">
	$(function(){ 
		CKEDITOR.replace( 'description_english' );
		CKEDITOR.replace( 'description_spanish' );
	})
</script>

@endsection