@extends('adminlte::layouts.app')

@section('htmlheader_title')
	
@endsection

@section('contentheader_title')
	Productos
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div clas="text-center"><h1>Productos para vender</h1></div>
		<!-- /.box -->
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Orden</th>
					<th>Nombre</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
		@foreach ($products as $product)
				<tr>
					<td>{{$product->weight}}</td>
					<td>{{$product->name_spanish}}</td>
					<td><a href="{{url('admin/products/edit/' . $product->id)}}"><button class="btn btn-info">Editar</button></a></td>
					<td><a href="{{url('admin/products/delete/' . $product->id)}}"><button class="btn btn-danger">Eliminar</button></a></td>
				</tr>
		@endforeach
			</tbody>
		</table>

		<a href="{{url('admin/products/create')}}"><button class="btn btn-success">Agregar Producto</button></a>
	</div>
@endsection


@section('custom_scripts')

<script type="text/javascript">
	$(function() {
		$('body').on('click', '.remove-image', function() {
			var $this = $(this)
			var $row = $this.closest('.slider-image')
			var data  = {
				field: $row.data('field'),
				index: $row.data('index')
			}
			$.ajax({
				method: 'POST',
				url: "{{url('admin/removeMetaWithIndex')}}",
				data: data
			}).done(function(data){
				if (data == "ok") {
					$row.remove()
				}
			})
		})
	})
</script>

@endsection