@extends('adminlte::layouts.app')

@section('htmlheader_title')
	
@endsection

@section('contentheader_title')
	Crear Marca
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<!-- /.box -->
		<div class="text-center"><h1>AGREGAR PRODUCTO</h1></div>
		{!! Form::open(['url' => 'admin/products/update/' . $product->id, 'files' => true]) !!}
			<h1> Español </h1>
			<div class="form-group">
				{!! Form::label('name_spanish', 'Nombre (Español') !!}
				{!! Form::text('name_spanish', $product->name_spanish) !!}
			</div>


			<div class="form-group">
			    {!! Form::label('Descripcion (Español') !!}
			    {!! Form::textarea('description_spanish', $product->description_spanish) !!}
			</div>

			<h1> Ingles </h1>
			<div class="form-group">
				{!! Form::label('name_english', 'Nombre (Ingles') !!}
				{!! Form::text('name_english', $product->name_english) !!}
			</div>

			<div class="form-group">
			    {!! Form::label('Descripcion (Ingles') !!}
			    {!! Form::textarea('description_english', $product->description_english) !!}
			</div>

			<div class="form-group">
				{!! Form::label('weight', 'Orden') !!}
				{!! Form::number('weight', $product->weight) !!}
			</div>

			<div class="form-group text-center">
				{!! Form::submit('Enviar') !!}
			</div>
		{!! Form::close() !!}
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Imagen</th>
						<th>Icono</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
				@foreach ($product->images as $image)
					<tr>
						<td><img style="width: 100px;" src="{{asset($image->slide)}}">
						<td><a href="{{url('admin/products/deleteImage/' . $image->id)}}"><button class="btn btn-danger">Eliminar</button></a></td>
					</tr>
				@endforeach
				</tbody>
			</table>
		{!! Form::open(['url' => 'admin/products/uploadImage/' . $product->id, 'files' => true]) !!}
			<div class="form-group row">
				<div class="col-xs-6">
				    {!! Form::label('Imagen') !!}
				    {!! Form::file('image', null) !!}
				</div>
			</div>
			<div class="form-group text-center">
				{!! Form::submit('Subir imagen') !!}
			</div>
		{!! Form::close() !!}

		
	</div>
@endsection


@section('custom_scripts')

<script type="text/javascript">
	$(function(){ 
		CKEDITOR.replace( 'description_english' );
		CKEDITOR.replace( 'description_spanish' );
	})
</script>

@endsection