@extends('adminlte::layouts.app')

@section('htmlheader_title')
	
@endsection

@section('contentheader_title')
	Crear Marca
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<!-- /.box -->
		<div class="text-center"><h1>Imagen indice de posts</h1></div>
		{!! Form::open(['url' => 'admin/updateMeta', 'files' => true]) !!}
		{!! Form::hidden('redirect' , 'admin/blog/index') !!}
			<div class="form-group row">
				<div class="col-xs-6">
			    {!! Form::label('Imagen') !!}
			    {!! Form::file('blog_image', null) !!}
				</div>
				<div class="col-xs-6">
					<img style="width: 100%; height: auto;" src="{{asset(getMeta('blog_image'))}}">
				</div>
			</div>
			<div class="form-group text-center">
				{!! Form::submit('Enviar') !!}
			</div>

		{!! Form::close() !!}
	</div>
@endsection


@section('custom_scripts')

<script type="text/javascript">
</script>

@endsection