@extends('adminlte::layouts.app')

@section('htmlheader_title')
	SOBRE HANGAR
@endsection

@section('contentheader_title')
	SOBRE HANGAR - Texto
@endsection
@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- /.box -->
				{!! Form::open(['url' => 'admin/updateMeta', 'files' => true]) !!}
				{!! Form::hidden('redirect' , 'admin/blog/fullpage') !!}

				<!-- Quienes Somos Main Izquierda -->
				<div class="form-group">
					{!! Form::label('blog_fullpage_link', 'Texto de link') !!}
					{!! Form::textarea('blog_fullpage_link', getMeta('blog_fullpage_link')) !!}
				</div>

				<!-- Quienes Somos Main Izquierda -->
				<div class="form-group">
					{!! Form::label('blog_fullpage_link_english', 'Texto de link (Ingles) ') !!}
					{!! Form::textarea('blog_fullpage_link_english', getMeta('blog_fullpage_link_english')) !!}
				</div>

				<div class="row">
					<div class="form-group col-xs-6">
					    {!! Form::label('Fondo') !!}
					    {!! Form::file('blog_link_fondo', null) !!}
					</div>
					<div class="col-xs-6">
						<img style="width: 100%; height: auto;" src="{{asset(getMeta('blog_link_fondo'))}}">
					</div>
				</div>

				<div class="form-group">
					{!! Form::submit('Enviar') !!}
				</div>
				{!! Form::close() !!}

			</div>
		</div>
	</div>
@endsection


@section('custom_scripts')

<script type="text/javascript">
$(function(){ 
		CKEDITOR.replace( 'blog_fullpage_link' );
		CKEDITOR.replace( 'blog_fullpage_link_english' );
	})
</script>

@endsection