@extends('adminlte::layouts.app')

@section('htmlheader_title')
	
@endsection

@section('contentheader_title')
	
@endsection
@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- /.box -->
				{!! Form::open(['url' => 'admin/updateMeta', 'files' => true]) !!}
				{!! Form::hidden('redirect' , 'admin/contact/addresses') !!}

				<!-- Quienes Somos Main Izquierda -->
				<div class="form-group">
					{!! Form::label('contact_addresses', 'Direcciones') !!}
					{!! Form::textarea('contact_addresses', getMeta('contact_addresses')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('contact_addresses_english', 'Direcciones (Ingles)') !!}
					{!! Form::textarea('contact_addresses_english', getMeta('contact_addresses_english')) !!}
				</div>

				<div class="form-group">
					{!! Form::submit('Enviar') !!}
				</div>
				{!! Form::close() !!}

			</div>
		</div>
	</div>
@endsection


@section('custom_scripts')

<script type="text/javascript">
	$(function(){ 
		CKEDITOR.replace( 'contact_addresses' );
		CKEDITOR.replace( 'contact_addresses_english' );
	})
</script>

@endsection