@extends('adminlte::layouts.app')

@section('htmlheader_title')
	SOBRE HANGAR
@endsection

@section('contentheader_title')
	SOBRE HANGAR - Texto
@endsection
@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- /.box -->
				{!! Form::open(['url' => 'admin/updateMeta', 'files' => true]) !!}
				{!! Form::hidden('redirect' , 'admin/ta/text') !!}

				<!-- Quienes Somos Main Izquierda -->
				<div class="form-group">
					{!! Form::label('ta_text', 'Trabajo Aereo - Texto') !!}
					{!! Form::textarea('ta_text', getMeta('ta_text')) !!}
				</div>


				<div class="form-group">
					{!! Form::label('ta_text_english', 'Trabajo Aereo - Texto (Ingles)') !!}
					{!! Form::textarea('ta_text_english', getMeta('ta_text_english')) !!}
				</div>

				<div class="form-group">
					{!! Form::submit('Enviar') !!}
				</div>
				{!! Form::close() !!}

			</div>
		</div>
	</div>
@endsection


@section('custom_scripts')

<script type="text/javascript">
	$(function(){ 
		CKEDITOR.replace( 'ta_text' );
		CKEDITOR.replace( 'ta_text_english' );
	})
</script>

@endsection