@extends('adminlte::layouts.app')

@section('htmlheader_title')
	
@endsection

@section('contentheader_title')
	Crear Marca
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<!-- /.box -->
		<div class="text-center"><h1>EDITAR MODELO</h1></div>
		{!! Form::open(['url' => 'admin/modelos/update/' . $modelo->id, 'files' => true]) !!}
			{!! Form::hidden('marca_id' , $modelo->marca->id) !!}
			<div class="form-group">
				{!! Form::label('name', 'Nombre') !!}
				{!! Form::text('name', $modelo->name) !!}
			</div>

			<div class="form-group">
				{!! Form::label('weight', 'Orden') !!}
				{!! Form::number('weight', $modelo->weight) !!}
			</div>

				
			<div class="row">
				<div class="form-group col-xs-6">
				    {!! Form::label('Imagen') !!}
				    {!! Form::file('image', null) !!}
				</div>
				<div class="col-xs-6">
					<img style="width: 100%; height: auto;" src="{{asset($modelo->image)}}">
				</div>
			</div>

			<div class="form-group">
			    {!! Form::label('PDF') !!}
			    {!! Form::file('pdf', null) !!}
			</div>


			<div class="form-group text-center">
				{!! Form::submit('Enviar') !!}
			</div>

		{!! Form::close() !!}
	</div>
@endsection


@section('custom_scripts')

<script type="text/javascript">
	$(function(){ 
		CKEDITOR.replace( 'description' );
	})
</script>

@endsection