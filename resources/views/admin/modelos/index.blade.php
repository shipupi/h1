@extends('adminlte::layouts.app')

@section('htmlheader_title')
	
@endsection

@section('contentheader_title')
	Slider
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div clas="text-center"><h1>Modelos de {{$marca->name}}</h1></div>
		<!-- /.box -->
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Orden</th>
					<th>Nombre</th>
					<th>Imagen</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
		@foreach ($marca->modelos as $modelo)
				<tr>
					<td>{{$modelo->weight}}</td>
					<td>{{$modelo->name}}</td>
					<td><img style="width: 100px;" src="{{asset($modelo->image)}}"></td>
					<td><a href="{{url('admin/modelos/edit/' . $modelo->id)}}"><button class="btn btn-info">Editar</button></a></td>
					<td><a href="{{url('admin/modelos/delete/' . $modelo->id)}}"><button class="btn btn-danger">Eliminar</button></a></td>
				</tr>
		@endforeach
			</tbody>
		</table>

		<a href="{{url('admin/modelos/create/' . $marca->id)}}"><button class="btn btn-success">Agregar Modelo</button></a>
	</div>
@endsection


@section('custom_scripts')

<script type="text/javascript">
	$(function() {
		$('body').on('click', '.remove-image', function() {
			var $this = $(this)
			var $row = $this.closest('.slider-image')
			var data  = {
				field: $row.data('field'),
				index: $row.data('index')
			}
			$.ajax({
				method: 'POST',
				url: "{{url('admin/removeMetaWithIndex')}}",
				data: data
			}).done(function(data){
				if (data == "ok") {
					$row.remove()
				}
			})
		})
	})
</script>

@endsection