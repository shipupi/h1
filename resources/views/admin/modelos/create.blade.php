@extends('adminlte::layouts.app')

@section('htmlheader_title')
	
@endsection

@section('contentheader_title')
	Crear Marca
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<!-- /.box -->
		<div class="text-center"><h1>AGREGAR MODELO</h1></div>
		{!! Form::open(['url' => 'admin/modelos/store', 'files' => true]) !!}
			{!! Form::hidden('marca_id' , $marca_id) !!}
			<div class="form-group">
				{!! Form::label('name', 'Nombre') !!}
				{!! Form::text('name') !!}
			</div>

			<div class="form-group">
				{!! Form::label('weight', 'Orden') !!}
				{!! Form::number('weight') !!}
			</div>
			<div class="form-group">
			    {!! Form::label('Imagen') !!}
			    {!! Form::file('image', null) !!}
			</div>

			<div class="form-group">
			    {!! Form::label('PDF') !!}
			    {!! Form::file('pdf', null) !!}
			</div>


			<div class="form-group text-center">
				{!! Form::submit('Enviar') !!}
			</div>

		{!! Form::close() !!}
	</div>
@endsection


@section('custom_scripts')

<script type="text/javascript">
	$(function(){ 
		CKEDITOR.replace( 'description' );
	})
</script>

@endsection