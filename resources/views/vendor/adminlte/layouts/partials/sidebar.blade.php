<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}</a>
                </div>
            </div>
        @endif

        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">{{ trans('adminlte_lang::message.header') }}</li>
            <!-- Optionally, you can add icons to the links -->

            <!-- HOME -->
            <li class="treeview">
                <a href="#"><i class='fa fa-home'></i> <span>Home</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{url('admin/home/verticalText')}}">Texto Vertical</a></li>
                    <li><a href="{{url('admin/home/slider')}}">Slider</a></li>
                </ul>
            </li>

            <!-- About -->
            <li class="treeview">
                <a href="#"><i class='fa fa-question-circle'></i> <span>About</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{url('admin/about/text')}}">Texto</a></li>
                    <li><a href="{{url('admin/about/slider')}}">Slider</a></li>
                </ul>
            </li>

            <!-- About -->
            <li class="treeview">
                <a href="#"><i class='fa fa-plane'></i> <span>Aeronaves</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{url('admin/marcas')}}">Marcas</a></li>
                </ul>
            </li>

            <!-- About -->
            <li class="treeview">
                <a href="#"><i class='fa fa-money'></i> <span>Productos en Venta</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{url('admin/products')}}">Productos</a></li>
                </ul>
            </li>

            <!-- Mantenimiento -->
            <li class="treeview">
                <a href="#"><i class='fa fa-wrench'></i> <span>Mantenimiento</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{url('admin/mantenimiento/text')}}">Texto</a></li>
                    <li><a href="{{url('admin/mantenimiento/slider')}}">Slider</a></li>
                </ul>
            </li>

            <!-- entrenamiento -->
            <li class="treeview">
                <a href="#"><i class='fa fa-soccer-ball-o'></i> <span>Entrenamiento</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{url('admin/entrenamiento/text')}}">Texto</a></li>
                    <li><a href="{{url('admin/entrenamiento/slider')}}">Slider</a></li>
                    <li><a href="{{url('admin/entrenamiento/manual')}}">PDF</a></li>
                </ul>
            </li>

            <!-- fbo -->
            <li class="treeview">
                <a href="#"><i class='fa fa-signing'></i> <span>FBO</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{url('admin/fbo/text')}}">Texto</a></li>
                    <li><a href="{{url('admin/fbo/slider')}}">Slider</a></li>
                </ul>
            </li>

            <!-- fbo -->
            <li class="treeview">
                <a href="#"><i class='fa fa fa-newspaper-o'></i> <span>Blog</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{url('admin/blog/fullpage')}}">Fullpage</a></li>
                    <li><a href="{{url('admin/blog/index')}}">Indice de Posts</a></li>
                    <li><a href="{{url('admin/posts')}}">Posts</a></li>
                </ul>
            </li>

            <!-- fbo -->
            <li class="treeview">
                <a href="#"><i class='fa fa-paper-plane'></i> <span>Trabajo Aereo</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{url('admin/ta/text')}}">Texto</a></li>
                    <li><a href="{{url('admin/ta/slider')}}">Slider</a></li>
                </ul>
            </li>

            <!-- fbo -->
            <li class="treeview">
                <a href="#"><i class='fa fa-envelope-o'></i> <span>Contacto</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{url('admin/contact/addresses')}}">Direcciones</a></li>
                </ul>
            </li>




        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
