<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <title> Hangar uno</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{ asset('/css/hangar1.css') }}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

	<div id="hangar1">
        <post jpost="{{json_encode($post)}}"></post>
	</div>
    <script src="{{ url ('js/jquery-3.2.1.min.js') }}" type="text/javascript"></script>
    <script src="{{ url (mix('/js/plugins.js')) }}" type="text/javascript"></script>
    <script src="{{ url (mix('/js/blog.js')) }}" type="text/javascript"></script>
</body>
</html>
<script type="text/javascript">
    $(document).ready(function() {
        $('#fullpage').fullpage({
            verticalCentered: false,
            css3: true,
            lazyloading: true,
        });
    });
</script>