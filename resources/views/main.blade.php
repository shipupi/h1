<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <title> Hangar uno</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{ asset('/css/hangar1.css') }}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <!-- youtube api -->
    <script>
      // 2. This code loads the IFrame Player API code asynchronously.
      var tag = document.createElement('script');
      var youtubePlayers = []
      var createYoutube, canSlide = true, homeSlider

      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

      // 3. This function creates an <iframe> (and YouTube player)
      //    after the API code downloads.
      function onYouTubeIframeAPIReady() {
        createYoutube = function(id, videoId) {
             var newPlayer = new YT.Player(id, {
              height: '390',
              width: '640',
              videoId: videoId,
              events: {
                // 'onReady': onPlayerReady,
                // 'onStateChange': onPlayerStateChange
              },
              playerVars: {
                'showinfo': 0,
                'controls': 0
            },
            });

            return newPlayer
        }
      }

      // 4. The API will call this function when the video player is ready.
      function onPlayerReady(event) {
        // event.target.playVideo();
      }

      // 5. The API calls this function when the player's state changes.
      //    The function indicates that when playing a video (state=1),
      //    the player should play for six seconds and then stop.
      var done = false;
      function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING && !done) {
          setTimeout(stopVideo, 6000);
          done = true;
        }
      }
      function stopVideo() {
        player.stopVideo();
      }
    </script>

	<div id="hangar1">

        <div id="fullpage">
            <home-slider class="section"></home-slider>
            <about class="section"></about>
            <aeronaves class="section"></aeronaves>
            <venta class="section"></venta>
            <entrenamiento class="section"></entrenamiento>
            <mantenimiento class="section"></mantenimiento>
            <fbo class="section"></fbo>
            <trabajo-aereo class="section"></trabajo-aereo>
            <blog class="section"></blog>
            <contacto class="section"></contacto>
        </div>


	</div>
    <script src="{{ url ('js/jquery-3.2.1.min.js') }}" type="text/javascript"></script>
    <script src="{{ url (mix('/js/plugins.js')) }}" type="text/javascript"></script>
    <script src="{{ url (mix('/js/h1.js')) }}" type="text/javascript"></script>
</body>
</html>
<script type="text/javascript">
    $(document).ready(function() {
        $('#fullpage').fullpage({
            verticalCentered: false,
            css3: true,
            lazyloading: true,
        });
    });
</script>