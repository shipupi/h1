<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@main');
Route::get('setLanguage/{lang}', 'HomeController@setLanguage');
Route::get('blog', 'HomeController@blog');
Route::get('blog/{id}', 'HomeController@post');
Route::get('api/getMeta', 'HomeController@getAllMeta');
Route::get('api/getMarcas', 'HomeController@getMarcas');
Route::get('api/getProducts', 'HomeController@getProducts');
Route::get('api/getPosts', 'HomeController@getPosts');
Route::post('api/sendMail', 'HomeController@sendMail');


Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {

	Route::get('/', 'AdminController@home');
	Route::post('updateMeta', 'AdminController@updateMeta');
	Route::post('removeMetaWithIndex', 'AdminController@removeMetaWithIndex');
	Route::group(['prefix' => 'home'], function () {
		Route::get('verticalText', 'AdminController@homeVerticalText');
		Route::get('slider', 'AdminController@homeSlider');
	});
	Route::group(['prefix' => 'about'], function () {
		Route::get('text', 'AdminController@aboutText');
		Route::get('slider', 'AdminController@aboutSlider');
	});

	Route::group(['prefix' => 'aeronaves'], function () {

	});
	Route::group(['prefix' => 'marcas'], function () {
		Route::get('/', 'AdminController@marcasIndex');
		Route::get('create', 'AdminController@marcasCreate');
		Route::post('store', 'AdminController@marcasStore');
		Route::get('edit/{id}', 'AdminController@marcasEdit');
		Route::get('delete/{id}', 'AdminController@marcasDelete');
		Route::post('update/{id}', 'AdminController@marcasUpdate');
	});

	Route::group(['prefix' => 'modelos'], function () {
		Route::get('index/{id}', 'AdminController@modelosIndex');
		Route::get('create/{id}', 'AdminController@modelosCreate');
		Route::post('store', 'AdminController@modelosStore');
		Route::get('edit/{id}', 'AdminController@modelosEdit');
		Route::get('delete/{id}', 'AdminController@modelosDelete');
		Route::post('update/{id}', 'AdminController@modelosUpdate');
	});

	Route::group(['prefix' => 'products'], function () {
		Route::get('/', 'AdminController@productsIndex');
		Route::get('create', 'AdminController@productsCreate');
		Route::post('store', 'AdminController@productsStore');
		Route::post('uploadImage/{product_id}', 'AdminController@uploadProductImage');
		Route::get('deleteImage/{image_id}', 'AdminController@deleteProductImage');
		Route::get('edit/{id}', 'AdminController@productsEdit');
		Route::get('delete/{id}', 'AdminController@productsDelete');
		Route::post('update/{id}', 'AdminController@productsUpdate');
	});
	Route::group(['prefix' => 'entrenamiento'], function () {
		Route::get('text', 'AdminController@entrenamientoText');
		Route::get('slider', 'AdminController@entrenamientoSlider');
		Route::get('manual', 'AdminController@entrenamientoManual');
	});
	Route::group(['prefix' => 'mantenimiento'], function () {
		Route::get('text', 'AdminController@mantenimientoText');
		Route::get('slider', 'AdminController@mantenimientoSlider');
	});
	Route::group(['prefix' => 'fbo'], function () {
		Route::get('text', 'AdminController@fboText');
		Route::get('slider', 'AdminController@fboSlider');
	});

	Route::group(['prefix' => 'blog'], function () {
		Route::get('fullpage', 'AdminController@blogFullpage');
		Route::get('index', 'AdminController@blogIndex');
	});
	Route::group(['prefix' => 'posts'], function () {
		Route::get('/', 'AdminController@postsIndex');
		Route::get('create', 'AdminController@postsCreate');
		Route::get('edit/{id}', 'AdminController@postsEdit');
		Route::post('store', 'AdminController@postsStore');
		Route::post('update/{id}', 'AdminController@postsUpdate');
		Route::get('delete/{id}', 'AdminController@postsDelete');
	});

	Route::group(['prefix' => 'ta'], function () {
		Route::get('text', 'AdminController@taText');
		Route::get('slider', 'AdminController@taSlider');
	});

	Route::group(['prefix' => 'contact'], function () {
		Route::get('addresses', 'AdminController@contactAddresses');
	});
	


    //    Route::get('/link1', function ()    {
//        // Uses Auth Middleware
//    });

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes
});
