<?php

use \App\Meta;

function getRawMeta($name)
{
	$meta = Meta::where('name', $name)->first();
	return $meta;
}
function getMeta($name)
{
	$meta = getRawMeta($name);
	if ($meta) {
		return $meta->value;
	} else {
		return null;
	}
}