<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modelo extends Model
{
    protected $table = "modelos";
    protected $fillable = ['name', 'image', 'pdf', 'marca_id', 'weight'];
    protected $hidden = ['created_at', 'updated_at', 'id', 'marca_id'];	

    public function marca()
    {
    	return $this->belongsTo('App\Marca', 'marca_id');
    }
}
