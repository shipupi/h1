<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marca extends Model
{
    protected $table = "marcas";
    protected $fillable = ['name', 'description', 'english_description',  'image', 'image_active', 'slider_image', 'slider_link', 'weight'];
    protected $hidden = ['created_at', 'updated_at', 'id'];	
    public function modelos()
    {
    	return $this->hasMany('App\Modelo', 'marca_id')->orderBy('weight');;
    }
}
