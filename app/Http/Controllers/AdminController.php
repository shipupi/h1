<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meta;
use App\Post;
use App\Product;
use App\ProductImage;
use Auth;


class AdminController extends Controller
{

    // HOME
    public function home()
    {
    	return view('admin.home');
    }

    public function homeSlider()
    {
        return view('admin.home.slider');
    }
    public function homeVerticalText()
    {
        return view('admin.home.verticaltext');
    }
    public function aboutText()
    {
        return view('admin.about.text');
    }
    public function aboutSlider()
    {
    	return view('admin.about.slider');
    }

    public function entrenamientoText()
    {
        return view('admin.entrenamiento.text');
    }
    public function entrenamientoSlider()
    {
        return view('admin.entrenamiento.slider');
    }

    public function entrenamientoManual()
    {
        return view('admin.entrenamiento.manual');
    }

    public function mantenimientoText()
    {
        return view('admin.mantenimiento.text');
    }
    public function mantenimientoSlider()
    {
        return view('admin.mantenimiento.slider');
    }

    public function blogFullpage()
    {
        return view('admin.blog.fullpage');
    }

    public function blogIndex()
    {
        return view('admin.blog.index');
    }

    public function fboText()
    {
        return view('admin.fbo.text');
    }
    public function fboSlider()
    {
        return view('admin.fbo.slider');
    }

    public function taText()
    {
        return view('admin.trabajoaereo.text');
    }
    public function taSlider()
    {
        return view('admin.trabajoaereo.slider');
    }

    public function contactAddresses()
    {
        return view('admin.contact.addresses');
    }

    public function marcasIndex()
    {
        $marcas = \App\Marca::orderBy('weight')->get();
        return view('admin.marcas.index', compact('marcas'));
    }

    public function marcasCreate()
    {
        return view('admin.marcas.create');
    }

    public function marcasEdit($id)
    {
        $marca = \App\Marca::findOrFail($id);
        return view('admin.marcas.edit', compact('marca'));
    }

    public function marcasDelete($id)
    {
        $marca = \App\Marca::findOrFail($id);
        foreach ($marca->modelos as $modelo) {
            $modelo->delete();
        }
        $marca->delete();
        return redirect('admin/marcas');
    }

    public function marcasStore(Request $request)
    {
        $input = $request->all();
        $marca = \App\Marca::create($request->all());

        $image = $request->file('image');
        $activeImage = $request->file('image_active');
        $sliderImage = $request->file('slider_image');
        if ($image) {
            $imageName = $image->getClientOriginalName();
            $pathDirectory = 'assets/uploads/marcas/';
            $fullPath = $pathDirectory . $imageName;
            $request->file('image')->move( base_path() . '/public/' . $pathDirectory, $imageName);
            $marca->image = $fullPath;
        }
        if ($activeImage) {
            $imageName = $activeImage->getClientOriginalName();
            $pathDirectory = 'assets/uploads/marcas/';
            $fullPath = $pathDirectory . $imageName;
            $request->file('image_active')->move( base_path() . '/public/' . $pathDirectory, $imageName);
            $marca->image_active = $fullPath;
        }
        if ($sliderImage) {
            $imageName = $sliderImage->getClientOriginalName();
            $pathDirectory = 'assets/uploads/marcas/';
            $fullPath = $pathDirectory . $imageName;
            $request->file('slider_image')->move( base_path() . '/public/' . $pathDirectory, $imageName);
            $marca->slider_image = $fullPath;
        }
        $marca->save();

        return redirect('admin/marcas/');
    }

    public function marcasUpdate($id, Request $request)
    {
        $input = $request->all();
        $marca = \App\Marca::findOrFail($id);
        $marca->name = $request->input('name');
        $marca->description = $request->input('description');
        $marca->english_description = $request->input('english_description');
        $marca->slider_link = $request->input('slider_link');
        $marca->weight = $request->input('weight');
        $image = $request->file('image');
        $activeImage = $request->file('image_active');
        $sliderImage = $request->file('slider_image');
        if ($image) {
            $imageName = $image->getClientOriginalName();
            $pathDirectory = 'assets/uploads/marcas/';
            $fullPath = $pathDirectory . $imageName;
            $request->file('image')->move( base_path() . '/public/' . $pathDirectory, $imageName);
            $marca->image = $fullPath;
        }
        if ($activeImage) {
            $imageName = $activeImage->getClientOriginalName();
            $pathDirectory = 'assets/uploads/marcas/';
            $fullPath = $pathDirectory . $imageName;
            $request->file('image_active')->move( base_path() . '/public/' . $pathDirectory, $imageName);
            $marca->image_active = $fullPath;
        }
        if ($sliderImage) {
            $imageName = $sliderImage->getClientOriginalName();
            $pathDirectory = 'assets/uploads/marcas/';
            $fullPath = $pathDirectory . $imageName;
            $request->file('slider_image')->move( base_path() . '/public/' . $pathDirectory, $imageName);
            $marca->slider_image = $fullPath;
        }
        $marca->save();

        return redirect('admin/marcas/');   
    }


    public function modelosIndex($id)
    {
        $marca = \App\Marca::findOrFail($id);
        return view('admin.modelos.index', compact('marca'));
    }

    public function modelosCreate($id)
    {
        $marca_id = $id;
        return view('admin.modelos.create', compact('marca_id'));
    }

    public function modelosEdit($id)
    {
        $modelo = \App\Modelo::findOrFail($id);
        return view('admin.modelos.edit', compact('modelo'));
    }

    public function modelosDelete($id)
    {
        $modelo = \App\Modelo::findOrFail($id);
        $marca_id = $modelo->marca->id;
        $modelo->delete();
        return redirect('admin/modelos/index/' . $marca_id);
    }

    public function modelosStore(Request $request)
    {
        $input = $request->all();
        $modelo = \App\Modelo::create($request->all());
        $image = $request->file('image');
        $pdf = $request->file('pdf');
        if ($image) {
            $imageName = $image->getClientOriginalName();
            $pathDirectory = 'assets/uploads/modelos/';
            $fullPath = $pathDirectory . $imageName;
            $request->file('image')->move( base_path() . '/public/' . $pathDirectory, $imageName);
            $modelo->image = $fullPath;
        }
        if ($pdf) {
            $imageName = $pdf->getClientOriginalName();
            $pathDirectory = 'assets/uploads/modelos/';
            $fullPath = $pathDirectory . $imageName;
            $request->file('pdf')->move( base_path() . '/public/' . $pathDirectory, $imageName);
            $modelo->pdf = $fullPath;
        }
        $modelo->save();

        return redirect('admin/modelos/index/' . $request->input('marca_id'));
    }

    public function postsIndex()
    {
        $posts = Post::orderBy('created_at', 'desc')->get();
        return view('admin.posts.index', compact('posts'));
    }

    public function postsCreate()
    {
        return view('admin.posts.create');
    }

    public function postsEdit($id)
    {
        $post = \App\Post::findOrFail($id);
        return view('admin.posts.edit', compact('post'));
    }

    public function postsDelete($id)
    {
        $post = \App\Post::findOrFail($id);
        $post->delete();
        return redirect('admin/posts');
    }

    public function postsStore(Request $request)
    {
        $input = $request->all();
        $post = \App\Post::create($request->all());
        $post->user_id = Auth::user()->id;
        $image = $request->file('image');
        if ($image) {
            $imageName = $image->getClientOriginalName();
            $pathDirectory = 'assets/uploads/posts/';
            $fullPath = $pathDirectory . $imageName;
            $request->file('image')->move( base_path() . '/public/' . $pathDirectory, $imageName);
            $post->image = $fullPath;
        }
        $post->save();

        return redirect('admin/posts');
    }
    public function postsUpdate($id, Request $request)
    {
        $input = $request->all();
        $post = \App\Post::findOrFail($id);
        $post->title = $request->input('title');
        $post->lede = $request->input('lede');
        $post->content = $request->input('content');
        $post->english_title = $request->input('english_title');
        $post->english_lede = $request->input('english_lede');
        $post->english_content = $request->input('english_content');
        $image = $request->file('image');
        if ($image) {
            $imageName = $image->getClientOriginalName();
            $pathDirectory = 'assets/uploads/posts/';
            $fullPath = $pathDirectory . $imageName;
            $request->file('image')->move( base_path() . '/public/' . $pathDirectory, $imageName);
            $post->image = $fullPath;
        }
        $post->save();

        return redirect('admin/posts');
    }

    public function modelosUpdate($id, Request $request)
    {
        $input = $request->all();
        $modelo = \App\Modelo::findOrFail($id);
        $modelo->name = $request->input('name');
        $modelo->weight = $request->input('weight');
        $image = $request->file('image');
        $pdf = $request->file('pdf');
        if ($image) {
            $imageName = $image->getClientOriginalName();
            $pathDirectory = 'assets/uploads/modelos/';
            $fullPath = $pathDirectory . $imageName;
            $request->file('image')->move( base_path() . '/public/' . $pathDirectory, $imageName);
            $modelo->image = $fullPath;
        }
        if ($pdf) {
            $imageName = $pdf->getClientOriginalName();
            $pathDirectory = 'assets/uploads/modelos/';
            $fullPath = $pathDirectory . $imageName;
            $request->file('pdf')->move( base_path() . '/public/' . $pathDirectory, $imageName);
            $modelo->pdf = $fullPath;
        }
        $modelo->save();

        return redirect('admin/modelos/index/' . $modelo->marca->id);   
    }

    public function updateMeta(Request $request)
    {
        $exceptions = ['_token', 'redirect', 'youtube', 'slider', 'image'];
        $files = ['entrenamientoPdf', 'entrenamientoPdf_english','blog_link_fondo', 'blog_image'];
        $exceptions = array_merge($exceptions, $files);
        $input = $request->all();

        foreach ($files as $i) {
            $image = $request->file($i);
             if ($image) {
                // Image exists, uploading
                $imageName = $image->getClientOriginalName();
                $pathDirectory = 'assets/uploads/meta/';
                $fullPath = $pathDirectory . $imageName;
                $request->file($i)->move( base_path() . '/public/' . $pathDirectory, $imageName);
                $meta = Meta::where('name', $i)->first();
                if (!$meta) {
                    $meta = new Meta();
                    $meta->name = $i;
                }
                $meta->value = $fullPath;
                $meta->save();

            }
        }

        foreach ($input as $field => $value) {
            if (!in_array($field, $exceptions)) {
                $meta = Meta::where('name', $field)->first();
                if (!$meta) {
                    $meta = new Meta();
                    $meta->name = $field;
                }
                if (!$value) {
                    $value = '';
                }
                $meta->value = $value;
                $meta->save();
            } else if ($field == "slider") {
                $image = $request->file('image');
                $field = $request->input('slider');
                $meta = Meta::where('name', $field)->first();
                if ($meta) {
                    $sliderArray = json_decode($meta->value);
                } else {
                    $meta = new Meta;
                    $meta->name = $field;
                    $sliderArray = [];
                }
                if ($image) {
                    $imageName = $image->getClientOriginalName();
                    $pathDirectory = 'assets/uploads/' . $field . '/';
                    $fullPath = $pathDirectory . $imageName;
                    $request->file('image')->move( base_path() . '/public/' . $pathDirectory, $imageName);
                    array_push($sliderArray, ['image' => $fullPath]);
                    $meta->value = json_encode($sliderArray);
                    $meta->save();
                } else if ($request->input('youtube')) {
                    array_push($sliderArray, ['image' => $request->input('youtube')]);
                    $meta->value = json_encode($sliderArray);
                    $meta->save();
                } else {
                    return 'No se pudo subir nuevo elemento';
                }
            }
        }
        return redirect($input['redirect']);
    }



    public function productsIndex()
    {
        $products = Product::orderBy('weight')->get();
        return view('admin.products.index', compact('products'));
    }

    public function productsCreate()
    {
        return view('admin.products.create');
    }

    public function productsEdit($id)
    {
        $product = \App\Product::findOrFail($id);
        return view('admin.products.edit', compact('product'));
    }

    public function productsDelete($id)
    {
        $product = \App\Product::findOrFail($id);
        $product->delete();
        return redirect('admin/products');
    }

    public function productsStore(Request $request)
    {
        $input = $request->all();
        if (!$input['weight']) {
            $input['weight'] = 1;
        }
        $product = \App\Product::create($input);
        $product->save();

        return redirect('admin/products/');
    }

    public function uploadProductImage(Request $request, $product_id)
    {
        $imageModel = new ProductImage();
        $image = $request->file('image');
        $icon = $request->file('icon');
        $imageModel->product_id = $product_id;
        if ($image) {
            $imageName = $image->getClientOriginalName();
            $pathDirectory = 'assets/uploads/products/images/';
            $fullPath = $pathDirectory . $imageName;
            $request->file('image')->move( base_path() . '/public/' . $pathDirectory, $imageName);
            $slidePath = $fullPath;
            $imageModel->slide = $slidePath;
        }
        $imageModel->save();
        return redirect('admin/products/edit/' . $product_id);
    }

    public function deleteProductImage($image_id)
    {

        $imageModel = ProductImage::findOrFail($image_id);
        $product_id = $imageModel->product_id;
        $imageModel->delete();
        return redirect('admin/products/edit/' . $product_id);
    }

    public function productsUpdate($id, Request $request)
    {


        $input = $request->all();
        if (!$input['weight']) {
            $input['weight'] = 1;
        }
        $product = \App\Product::findOrFail($id);
        $product->name_spanish = $input['name_spanish'];
        $product->description_spanish = $input['description_spanish'];
        $product->name_english = $input['name_english'];
        $product->description_english = $input['description_english'];
        $product->weight = $input['weight'];

        $image = $request->file('image');
        $icon = $request->file('icon');
        if ($image) {
            $imageName = $image->getClientOriginalName();
            $pathDirectory = 'assets/uploads/products/images/';
            $fullPath = $pathDirectory . $imageName;
            $request->file('image')->move( base_path() . '/public/' . $pathDirectory, $imageName);
            $product->image = $fullPath;
        }
        if ($icon) {
            $imageName = $icon->getClientOriginalName();
            $pathDirectory = 'assets/uploads/products/icons/';
            $fullPath = $pathDirectory . $imageName;
            $request->file('icon')->move( base_path() . '/public/' . $pathDirectory, $imageName);
            $product->icon = $fullPath;
        }
        $product->save();

        return redirect('admin/products/');
    }


    public function removeMetaWithIndex()
    {
        $field = $_POST['field'];
        $index = $_POST['index'];
        $meta = Meta::where('name', $field)->first();
        $sliderArray = json_decode($meta->value);
        array_splice($sliderArray, $index,1);
        $meta->value = json_encode($sliderArray);
        $meta->save();

        echo "ok";
        return;
    }
}
