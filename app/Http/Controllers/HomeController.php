<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Meta;
use Cookie;
use Mail;
/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        return redirect('/');
    }
    public function main()
    {
        return view('main');
    }

    public function blog()
    {
        $posts = \App\Post::orderBy('created_at', 'desc')->get();
        return view('blog', compact('posts'));
    }

    public function post($id)
    {
        $post = \App\Post::findOrFail($id);
        return view('post', compact('post'));
    }
    public function getAllMeta(Request $request)
    {

        $metas = Meta::select('name','value')->get();
        $parsedMetas = [];
        foreach ($metas as $meta) {
            $parsedMetas[$meta->name] = $meta->value;
        }
        return ['metas' => $parsedMetas, 'lang' => $this->getLanguage($request)];
    }
    public function getMarcas()
    {
        $marcas = \App\Marca::with('modelos')->orderBy('weight')->get();
        return $marcas;
    }

    public function getProducts()
    {
        $products = \App\Product::with('images')->orderBy('weight')->get();
        return $products;
    }

    public function getPosts()
    {
        $posts = \App\Post::orderBy('created_at', 'desc')->get();
        return $posts;
    }

    public function getLanguage(Request $request)
    {
        if ($request->cookie('lang')) {
            return $request->cookie('lang');
        } else {
            return 'spanish';
        }
    }

    public function setLanguage($newLang)
    {
        $languages = ['spanish', 'english'];
        if (in_array($newLang, $languages)) {
            $cookie = Cookie::forever('lang', $newLang);
            return redirect('/')->withCookie($cookie);
        } else {
            return redirect('/');
        }
    }

    public function sendMail(Request $request)
    {
        // Mandar Mail
        $datos = $request->all();
        Mail::send('emails.contacto', ['datos' => $datos], function ($m) use ($datos) {
            $m->from('hangaruno1@gmail.com', 'Hangar uno');

            $m->to('INFO@HANGARUNO.COM.AR', 'Hangar uno')->subject('Formululario de contacto');
        });
        return;
    }
}