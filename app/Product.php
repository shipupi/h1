<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $fillable = ['name_spanish', 'name_english', 'description_spanish', 'description_english', 'image', 'icon', 'weight'];
    protected $hidden = ['created_at', 'updated_at', 'id'];

    public function images()
    {
    	return $this->hasMany('App\ProductImage');
    }
}
