<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	protected $fillable = ['title', 'user_id', 'content', 'image', 'lede', 'english_content', 'english_title', 'english_lede'];
	public function user()
	{
		return $this->belongsTo('App\User');
	}
}
