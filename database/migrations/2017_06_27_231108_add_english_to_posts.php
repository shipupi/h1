<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEnglishToPosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->string('english_title')->nullable();
            $table->longText('english_lede')->nullable();
            $table->longText('english_content')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('posts', function (Blueprint $table) {
            $table->dropColumn('english_title');
            $table->dropColumn('english_lede');
            $table->dropColumn('english_content');
        });
    }
}
