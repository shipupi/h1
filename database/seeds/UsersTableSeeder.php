<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create(['name' => 'brian', 'email' => 'ail.brianez@gmail.com', 'password' => bcrypt('1234')]);
        \App\User::create(['name' => 'Eke', 'email' => 'esnaola.ef@gmail.com', 'password' => bcrypt('1234')]);
    }
}
