-- MySQL dump 10.13  Distrib 5.7.18, for Linux (x86_64)
--
-- Host: localhost    Database: h1
-- ------------------------------------------------------
-- Server version	5.7.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `marcas`
--

DROP TABLE IF EXISTS `marcas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marcas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_active` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slider_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slider_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `english_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marcas`
--

LOCK TABLES `marcas` WRITE;
/*!40000 ALTER TABLE `marcas` DISABLE KEYS */;
INSERT INTO `marcas` VALUES (2,'Hona Jet','assets/uploads/marcas/HangarUnoWeb_0003s_0001s_0002s_0011_HONDA-JET-LOGO-.png','assets/uploads/marcas/HangarUnoWeb_0003s_0001s_0002s_0011_HONDA-JET-LOGO-.png','<p>La experimentaci&oacute;n, la exploraci&oacute;n y la investigaci&oacute;n de&nbsp;HondaJet fue realizada con innovaciones del dise&ntilde;o que crean un avi&oacute;n integrado y&nbsp;tecnol&oacute;gicamente avanzado.</p>','2017-06-12 03:24:22','2017-08-07 18:08:09','assets/uploads/marcas/HangarUnoWeb_0003s_0001s_0013_Vector-Smart-Object.png',NULL,NULL,1),(3,'Piper Aircraft','assets/uploads/marcas/PIPER LOGO.png','assets/uploads/marcas/PIPER LOGO color.png','<p>Piper Aircraft es el &uacute;nico fabricante de aviaci&oacute;n general que ofrece una l&iacute;nea completa de aviones. Desde aviones robustos y confiables hasta un sofisticado turboh&eacute;lice de alto rendimiento. Con base en Vero Beach, Florida.<br />\r\n&nbsp;</p>','2017-06-12 16:40:55','2017-08-07 18:08:31','assets/uploads/marcas/HangarUnoWeb_0003s_0001s_0012_Vector-Smart-Object.png','http://www.piper.com/',NULL,2),(4,'Robinson Helicopter','assets/uploads/marcas/HangarUnoWeb_0003s_0001s_0002s_0015_ROBINSON-SELECCION.png','assets/uploads/marcas/robinson logo color.png','<p>Robinson Helicopter es garant&iacute;a de calidad y seguridad en el aire por m&aacute;s de 40 a&ntilde;os.<br />\r\n&nbsp;</p>','2017-06-12 17:11:58','2017-07-20 16:54:31','assets/uploads/marcas/HangarUnoWeb_0003s_0001s_0011_Vector-Smart-Object.png',NULL,NULL,4),(6,'Quest','assets/uploads/marcas/HangarUnoWeb_0003s_0001s_0002s_0009_QUEST-copy.png','assets/uploads/marcas/HangarUnoWeb_0003s_0001s_0002s_0010_QUEST.png','<p>&nbsp;</p>\r\n\r\n<p>Construido resistente y seguro con comfort, capacidad y econom&iacute;a para uso comercial o personal.&nbsp;Un sue&ntilde;o para volar,&nbsp;muchas misiones. Un avi&oacute;n.</p>','2017-07-03 14:55:11','2017-07-20 16:54:15','assets/uploads/marcas/HangarUnoWeb_0003s_0001s_0010_Layer-79.png',NULL,'<p>&nbsp;</p>\r\n\r\n<p>Built tough&nbsp;and safe with comfort, capacity and economy for commercial or personal use. A dream to fly, many missions. One&nbsp;plane.</p>',3);
/*!40000 ALTER TABLE `marcas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meta`
--

DROP TABLE IF EXISTS `meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meta` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meta`
--

LOCK TABLES `meta` WRITE;
/*!40000 ALTER TABLE `meta` DISABLE KEYS */;
INSERT INTO `meta` VALUES (1,'home_slider','[{\"image\":\"assets\\/uploads\\/home_slider\\/_MG_9522.jpg\"},{\"image\":\"EhfkIbftI00\"},{\"image\":\"assets\\/uploads\\/home_slider\\/DJI_00531.jpg\"}]','2017-06-11 22:08:39','2017-08-07 19:33:14'),(2,'about_text','<p><strong>HANGAR UNO</strong> es una empresa Argentina dedicada a la comercializaci&oacute;n, mantenimiento y operaci&oacute;n de aeronaves. <strong>Consolidada como la empresa l&iacute;der en la venta de aviones y helic&oacute;pteros en el pa&iacute;s,</strong> Hangar Uno, cuenta con m&aacute;s de 30 a&ntilde;os en el mercado argentino y m&aacute;s de 300 aeronaves vendidas.</p>\r\n\r\n<p>Es representante exclusivo de Honda Aircraft Company, Robinson Helicopter Company,&nbsp; Piper Aircrafts y Quest Aircraft, para Argentina, Uruguay y Paraguay.</p>\r\n\r\n<p>Con sede en el aeropuerto internacional de San Fernando, Hangar Uno ofrece servicios t&eacute;cnicos oficiales y hangaraje con una capacidad de m&aacute;s de 15.000mts2. Cuenta tambi&eacute;n con las certificaciones de seguridad y calidad ISO 9001:2015.</p>\r\n\r\n<p>El centro de entrenamiento para pilotos de Hangar Uno, ofrece capacitaci&oacute;n para aspirantes y pilotos de todos los niveles. La escuela cuenta con simuladores para avi&oacute;n y helic&oacute;ptero homologados por la ANAC e instructores con m&aacute;s de 6.000 horas de vuelo.</p>','2017-06-11 22:09:14','2017-07-13 17:27:30'),(3,'mantenimiento_text','<p>El Centro de Mantenimiento de HANGAR UNO est&aacute; integrado por un grupo altamente calificado de profesionales, con amplia experiencia en todo tipo de helic&oacute;pteros y aviones. Asimismo, las elevadas exigencias que impone la tecnolog&iacute;a actual hacen que dicho personal se mantenga en una continua capacitaci&oacute;n mediante cursos de actualizaci&oacute;n en los Estados Unidos y Europa.<br />\r\n<br />\r\nEl Centro est&aacute; habilitado en la actualidad para la atenci&oacute;n de aeronaves de las marcas Robinson, Piper, Embraer y Honda Aircraft Company.</p>\r\n\r\n<p>&nbsp;</p>','2017-06-11 22:09:52','2017-06-12 16:06:32'),(4,'entretenimiento_text','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis cursus consectetur euismod. Pellentesque lobortis at diam feugiat egestas. Vestibulum eget eros ornare tellus bibendum dictum ac ut orci. Maecenas lacinia eleifend risus, non aliquam massa condimentum id. Nunc eu arcu lacus. Sed sed bibendum erat. Praesent sodales lobortis lacus eu scelerisque.</p>\r\n\r\n<p>Proin varius at diam quis elementum. Maecenas ornare et lectus sed sagittis. Vivamus at faucibus lacus. Nulla accumsan egestas felis, non viverra urna imperdiet a. Sed auctor elementum neque id euismod. Maecenas maximus commodo turpis, vitae blandit mi maximus eu. Curabitur venenatis magna nunc, vel volutpat nisi aliquet non. Vestibulum rutrum posuere leo, eu ultricies risus lacinia at. Nunc eu accumsan dolor, at tempor odio.</p>\r\n\r\n<p>Maecenas ultrices facilisis tortor, dictum sagittis diam maximus vel. Vivamus imperdiet, odio quis sagittis molestie, nisi lorem lacinia purus, vel pharetra felis enim a ante. Nunc lacus tellus, placerat ut varius euismod, convallis quis neque. Etiam in lobortis quam. Curabitur aliquam luctus sapien, in imperdiet nisi tempor ut. Ut sed finibus turpis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed euismod arcu nec velit fringilla vehicula. Pellentesque suscipit tincidunt tortor, a vehicula massa blandit in. Morbi in nulla augue. In aliquet lacinia turpis in commodo. Cras eu purus non risus vehicula accumsan. Maecenas semper dapibus lacinia. Suspendisse porta nulla odio. Ut at dolor placerat, maximus massa quis, finibus ligula. Integer faucibus, dui eget convallis tincidunt, dolor nisl imperdiet felis, et vestibulum metus nulla ut nulla.</p>','2017-06-11 22:09:56','2017-06-11 22:09:56'),(5,'entrenamiento_text','<p>Con m&aacute;s de 55.000 horas de vuelo y m&aacute;s de 1.500 pilotos de helic&oacute;ptero y avi&oacute;n formados, el centro entrenamiento de HANGAR UNO, es la instituci&oacute;n de mayor actividad de vuelo en su tipo en el pa&iacute;s.<br />\r\n<br />\r\nSu plantel de instructores est&aacute; formado por pilotos profesionales, provenientes de nuestras Fuerzas Armadas, con una experiencia individual superior a las 8.000 horas en instrucci&oacute;n.<br />\r\n<br />\r\nLos cursos son te&oacute;rico-pr&aacute;cticos, utilizando los &uacute;ltimos avances en metodolog&iacute;a did&aacute;ctica y t&eacute;cnicas de instrucci&oacute;n, tanto durante el vuelo como en las clases te&oacute;ricas.</p>','2017-06-11 22:10:59','2017-07-13 13:45:33'),(6,'fbo_text','<p>HANGAR UNO&nbsp;es una empresa l&iacute;der en el sector de la aviaci&oacute;n ejecutiva. Formada por un equipo de profesionales altamente capacitados, opera las 24 hs. los 365 d&iacute;as del a&ntilde;o, en una &aacute;rea de 15.000 m2 de modernas instalaciones. Regida por estrictas normas de seguridad garantiza la mayor eficiencia en la operaci&oacute;n de su vuelo privado o corporativo. Cuenta con:</p>\r\n\r\n<p>&raquo; Servicio de rampa.<br />\r\n&raquo; GPU, limpieza de cabina y asistencia en tierra.<br />\r\n&raquo; 7.000 m2 de hangares y 8.000 m2 de rampa para el alojamiento de aeronaves.<br />\r\n&raquo; Terminal privada de pasajeros.<br />\r\n&raquo; Sala de plan de vuelo equipada con sistema de autom&aacute;tico de detecci&oacute;n del tiempo.<br />\r\n&raquo; Sala de conferencia con video y audio.<br />\r\n&raquo; Confortable sala de pilotos y pasajeros.<br />\r\n&raquo; Disponibilidad de caf&eacute;, t&eacute; y hielo.<br />\r\n&raquo; WI-FI internet, fotocopiadora y fax.<br />\r\n&raquo; Catering profesional (con previa anticipaci&oacute;n).<br />\r\n&raquo; Asistencia inmediata para el alojamiento y alquiler de veh&iacute;culos de alta gama.<br />\r\n&raquo; Despacho de aeronaves.<br />\r\n&raquo; Reabastecimiento de combustible y servicio de limpieza de aeronaves</p>','2017-06-11 22:11:05','2017-07-13 13:46:39'),(7,'contact_addresses','<p><strong>INFORMES:</strong></p>\r\n\r\n<p>INFO@HANGARUNO.COM.AR</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>VENTAS:</strong></p>\r\n\r\n<p>VENTAS@HANGARUNO.COM.AR</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>ESCUELA DE VUELO:</strong></p>\r\n\r\n<p>TRAININGCENTER@HANGARUNO.COM.AR</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>MANTENIMIENTO:</strong></p>\r\n\r\n<p>MANT@HANGARUNO.COM.AR</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>TELEFONOS:</strong></p>\r\n\r\n<p>(54-11) 4714-8100 / 0770</p>\r\n\r\n<p>(54-11) 4005-7557 / 7878 / 7799</p>\r\n\r\n<p>FAX: (54-11) 4714-8600</p>','2017-06-11 22:12:44','2017-07-23 17:13:55'),(8,'about_slider','[{\"image\":\"assets\\/uploads\\/about_slider\\/32.png\"}]','2017-06-11 22:13:09','2017-06-12 18:06:33'),(9,'entrenamiento_slider','[{\"image\":\"assets\\/uploads\\/entrenamiento_slider\\/IMG_0168.jpg\"},{\"image\":\"assets\\/uploads\\/entrenamiento_slider\\/IMG_9498.jpg\"},{\"image\":\"assets\\/uploads\\/entrenamiento_slider\\/entrenamiento.png\"}]','2017-06-11 22:13:29','2017-07-17 20:27:55'),(10,'fbo_slider','[{\"image\":\"assets\\/uploads\\/fbo_slider\\/DJI_0015.jpg\"},{\"image\":\"assets\\/uploads\\/fbo_slider\\/fbo 1.png\"}]','2017-06-11 22:13:42','2017-07-11 14:35:03'),(11,'mantenimiento_slider','[{\"image\":\"assets\\/uploads\\/mantenimiento_slider\\/_MG_9446.jpg\"},{\"image\":\"assets\\/uploads\\/mantenimiento_slider\\/taller.png\"},{\"image\":\"assets\\/uploads\\/mantenimiento_slider\\/taller 2\"}]','2017-06-11 22:15:01','2017-07-17 17:48:44'),(12,'ta_slider','[{\"image\":\"assets\\/uploads\\/ta_slider\\/_MG_6886.jpg\"},{\"image\":\"assets\\/uploads\\/ta_slider\\/IMG_9346.jpg\"}]','2017-06-19 01:55:08','2017-07-10 13:05:38'),(13,'blog_image','assets/uploads/meta/IMG_6695 (1).jpg','2017-07-10 13:10:11','2017-07-10 13:10:11'),(14,'blog_link_fondo','assets/uploads/meta/GOPR6285.jpg','2017-07-10 19:17:31','2017-07-13 14:33:16'),(15,'blog_fullpage_link','<p><span class=\"marker\">Ingres&aacute; aqu&iacute; para ver todas las noticias de Hangar Uno</span></p>','2017-07-10 19:17:32','2017-07-13 14:35:09'),(16,'blog_fullpage_link_english','','2017-07-10 19:17:32','2017-07-10 19:17:32'),(17,'about_text_english','<p>HANGAR UNO is an Argentine company dedicated to sales, maintenance and operation of aircrafts. It has consolidated as the leading company for&nbsp; airplanes and helicopters sales in the country,&nbsp; with 30+ years of experience in the Argentine market and than 300+ aircraft sold.</p>\r\n\r\n<p>Hangar Uno represents is the official agent of Honda Aircraft Company, Robinson Helicopter Company and Piper Aircrafts, for Argentina, Uruguay and Paraguay.</p>\r\n\r\n<p>With headquarters in the San Fernando International Airport, Hangar Uno offers official technical service and FBO with 15,000mts2+. Hangar Uno also has the certifications of safety and quality ISO 9001:2015.</p>\r\n\r\n<p>The Hangar Uno pilot training center, offers training for aspirants and pilots of all levels. The school has an ANAC approved simulator and instructor teachers with more than 6.000 flight hours.</p>','2017-07-13 13:43:29','2017-07-13 17:27:30'),(18,'mantenimiento_text_english','<p>The HANGAR UNO Maintenance Center is composed by highly qualified group of professionals, with wide experience in all types of helicopters and airplanes. Likewise, the high demands imposed by current technology mean that these personnel are kept in continuous training through refresher courses in the United States and Europe.</p>\r\n\r\n<p>The Center is currently enabled for attention to Robinson Helicopter Company, Piper Aircrafts, Embraer and Honda Aircraft Company.</p>','2017-07-13 13:44:28','2017-07-13 13:44:28'),(19,'entrenamiento_text_english','<p>With more than 55,000 flight hours and more than 1,500 helicopter pilots and aircraft formed, the HANGAR UNO training center is the largest flying activity institution of its kind in the country.</p>\r\n\r\n<p>Its instructor staff is formed by professional pilots from our Armed Forces, with an individual experience exceeding 8,000 hours in instruction.</p>\r\n\r\n<p>The courses are theoretical-practical, using the latest advances in didactic methodology and instructional techniques, both during flight and in theoretical classes.</p>','2017-07-13 13:45:33','2017-07-13 13:45:33'),(20,'fbo_text_english','<p>HANGAR UNO is the only fixed base operator in the region, with a highly qualified staff that operates during 24/7, all year long. In its San Fernando headquarters, it has 150,000 ft. modern facilities, built under the latest and most stringent industry standards that assures quality service for aircraft owners and operators.</p>\r\n\r\n<p>&raquo; Ramp Service.<br />\r\n&raquo; International dispatch Services,<br />\r\n&raquo; Ground power units<br />\r\n&raquo; Interior and exterior cleaning services<br />\r\n&raquo; Catering<br />\r\n&raquo; Flight planning<br />\r\n&raquo; Passenger lounges<br />\r\n&raquo; Conference rooms with video and audio systems<br />\r\n&raquo; Showers<br />\r\n&raquo; Aircraft storage facilities<br />\r\n&raquo; Tow tractors<br />\r\n&raquo; Hotel arrangements and Limo Services<br />\r\n&raquo; On site refueling<br />\r\n&raquo; Free high speed internet access</p>','2017-07-13 13:46:39','2017-07-13 13:46:39'),(21,'ta_text','<p>La diversidad de actividades que comprende el Trabajo A&eacute;reo es consecuencia de la gran versatilidad de los las aeronaves utilizadas , estas actividades est&aacute;n mayormente comprendidas por:</p>\r\n\r\n<p>&raquo; Transporte Ejecutivo.<br />\r\n&raquo; Transporte de Carga (interna o externa).<br />\r\n&raquo; Seguridad y Vigilancia A&eacute;rea.<br />\r\n&raquo; Transporte de caudales y/o mercanc&iacute;as.&nbsp;<br />\r\n&raquo; Publicidad.<br />\r\n&raquo; Filmaciones A&eacute;reas.<br />\r\n&raquo; Aerofotograf&iacute;a Filmaci&oacute;n y control de eventos deportivos.<br />\r\n&raquo; Seguimiento, Inspecci&oacute;n y Control de L&iacute;neas de Alta Tensi&oacute;n.<br />\r\n&raquo; Prospecci&oacute;n geol&oacute;gica. Inspecci&oacute;n de redes y gasoductos.</p>','2017-07-13 13:48:54','2017-07-13 13:48:54'),(22,'ta_text_english','<p>The great versatility of the aircraf used conceves a wide diversity of activities, such us:</p>\r\n\r\n<p>&raquo; Executive transport.<br />\r\n&raquo; Cargo transport (internal or external).<br />\r\n&raquo; Air security and surveillance task.<br />\r\n&raquo; Funds and/or goods transport.<br />\r\n&raquo; Advertising.<br />\r\n&raquo; Air photography, filming and control of sport events.<br />\r\n&raquo; High voltage power line control and inspection.<br />\r\n&raquo; Geological prospecting, gas and oil pipelines inspection.</p>','2017-07-13 13:48:54','2017-07-13 13:48:54'),(23,'entrenamientoPdf','assets/uploads/meta/preguntas escuela de vuelo.pdf','2017-07-13 14:16:21','2017-07-13 14:16:21'),(24,'entrenamientoPdf_english','assets/uploads/meta/preguntas escuela de vuelo.pdf','2017-07-13 14:16:21','2017-07-13 14:16:21'),(25,'contact_addresses_english','','2017-07-14 22:15:02','2017-07-14 22:15:02');
/*!40000 ALTER TABLE `meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2017_06_04_154045_create_pages_meta_sistem',1),(4,'2017_06_12_002351_create_marcas_table',2),(5,'2017_06_13_032746_add_homeslider_to_marcas',3),(6,'2017_06_20_175037_create_posts_table',4),(7,'2017_06_27_231108_add_english_to_posts',4),(8,'2017_06_29_012726_add_english_to_marcas',4),(9,'2017_07_10_132949_add_weight_to_marcas',5),(10,'2017_07_10_135515_add_weight_to_modelos',6);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modelos`
--

DROP TABLE IF EXISTS `modelos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modelos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `marca_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pdf` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `weight` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modelos`
--

LOCK TABLES `modelos` WRITE;
/*!40000 ALTER TABLE `modelos` DISABLE KEYS */;
INSERT INTO `modelos` VALUES (1,2,'HA 420','assets/uploads/modelos/ha420slider.png','assets/uploads/modelos/hondajet-brochure.pdf','2017-06-12 03:24:49','2017-06-12 17:02:30',1),(2,3,'Archer LX','assets/uploads/modelos/2-plane.jpg','assets/uploads/modelos/Piper Archer LX.pdf','2017-06-12 17:18:00','2017-07-11 13:34:38',4),(4,4,'R 22','assets/uploads/modelos/r22','assets/uploads/modelos/r22_brochure.pdf','2017-06-12 17:38:41','2017-07-16 21:18:47',1),(5,4,'R 44 CADET','assets/uploads/modelos/r44.png','assets/uploads/modelos/r44_cadet_brochure.pdf','2017-06-12 17:40:49','2017-07-16 21:19:28',1),(6,4,'R 44 RAVEN I','assets/uploads/modelos/R44 raven I','assets/uploads/modelos/r44_raven_1_brochure.pdf','2017-06-12 17:43:11','2017-07-16 21:22:20',1),(7,4,'R 44 RAVEN II','assets/uploads/modelos/R44 raven II','assets/uploads/modelos/r44_raven_2_brochure.pdf','2017-06-12 17:47:28','2017-07-16 21:22:58',1),(8,4,'R 66','assets/uploads/modelos/R66.png','assets/uploads/modelos/r66_turbine_brochure.pdf','2017-06-12 17:51:57','2017-07-16 21:23:30',1),(9,3,'M 350','assets/uploads/modelos/M350Header-1024x413.jpg','assets/uploads/modelos/M350_2016Brochure_LR.pdf','2017-06-12 17:58:30','2017-07-11 13:33:40',3),(10,3,'M 500','assets/uploads/modelos/m500.jpg','assets/uploads/modelos/M500_2016Brochure_LR.pdf','2017-06-12 18:00:33','2017-07-11 13:33:06',2),(11,3,'M 600','assets/uploads/modelos/m600.jpg','assets/uploads/modelos/M600_2016BrochureLR.pdf','2017-06-12 18:02:36','2017-07-10 14:58:47',1),(12,6,'Kodiak','assets/uploads/modelos/kodiak.png','assets/uploads/modelos/Quest-Kodiak-Brochure-2016.pdf','2017-07-03 15:18:12','2017-07-16 21:35:54',1),(13,3,'Seneca V','assets/uploads/modelos/Captura de pantalla 2017-07-11 a las 10.36.36 a.m..png','assets/uploads/modelos/Seneca Brochure - Piper.pdf','2017-07-11 13:36:58','2017-07-11 13:36:58',5);
/*!40000 ALTER TABLE `modelos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `lede` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `english_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `english_lede` longtext COLLATE utf8mb4_unicode_ci,
  `english_content` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'brian','ail.brianez@gmail.com','$2y$10$NXhB66T8NrvWJujjhWHf9.G4qTyhC4vXQ9E2RQryn35GqbZir.eZu',NULL,'2017-06-11 22:06:42','2017-06-11 22:06:42'),(2,'Eke','esnaola.ef@gmail.com','$2y$10$awmZgyZ.C0hrDTlP.ttfyutr0fRHYXxylH3XHHSlYYOlOtI/6QTjW',NULL,'2017-06-11 22:06:43','2017-06-11 22:06:43');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-17 14:04:08
